package com.groupe3.jeeserver.exception;

public class DocumentSetNotFoundException extends ResourceNotFoundException {
    public DocumentSetNotFoundException(String fieldName, Object fieldValue) {
        super("DocumentSet", fieldName, fieldValue);
    }
}
