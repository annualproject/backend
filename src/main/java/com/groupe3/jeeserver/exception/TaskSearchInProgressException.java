package com.groupe3.jeeserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class TaskSearchInProgressException extends RuntimeException {
    public TaskSearchInProgressException(String searchQuery) {
        super("The search " + searchQuery + " is already in progress");
    }
}
