package com.groupe3.jeeserver.exception;

public class TaskSearchNotFoundException extends ResourceNotFoundException{
    public TaskSearchNotFoundException(String fieldName, Object fieldValue) {
        super("TaskSearch", fieldName, fieldValue);
    }
}
