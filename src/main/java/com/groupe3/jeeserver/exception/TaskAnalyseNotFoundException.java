package com.groupe3.jeeserver.exception;

public class TaskAnalyseNotFoundException extends ResourceNotFoundException{

    public TaskAnalyseNotFoundException(String fieldName, Object fieldValue) {
        super("TaskAnalyse", fieldName, fieldValue);
    }
}
