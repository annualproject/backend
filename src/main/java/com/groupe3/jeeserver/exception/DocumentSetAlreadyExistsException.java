package com.groupe3.jeeserver.exception;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class DocumentSetAlreadyExistsException extends RuntimeException {
    public DocumentSetAlreadyExistsException(String documentSet) {
        super("Document set " + documentSet + " already exists");
    }
}

