package com.groupe3.jeeserver.exception;

public class UserNotFoundException extends ResourceNotFoundException {
    public UserNotFoundException(String fieldName, Object fieldValue) {
        super("Subject", fieldName, fieldValue);
    }
}
