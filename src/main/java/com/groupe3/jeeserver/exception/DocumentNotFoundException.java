package com.groupe3.jeeserver.exception;


public class DocumentNotFoundException extends ResourceNotFoundException {
    public DocumentNotFoundException(String fieldName, Object fieldValue) {
        super("Document", fieldName, fieldValue);
    }
}
