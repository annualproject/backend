package com.groupe3.jeeserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

import java.io.IOException;
import java.util.concurrent.TimeoutException;


@SpringBootApplication
@EnableJpaAuditing
public class JeeServerApplication {

    public static void main(String[] args) throws IOException, TimeoutException {
        SpringApplication.run(JeeServerApplication.class, args);



    }

}
