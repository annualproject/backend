package com.groupe3.jeeserver.taskAnalyse.repository;

import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskAnalyseRepository extends JpaRepository<TaskAnalyse, Long> {
    Optional<TaskAnalyse> findByUuid(String uuid);
    Optional<List<TaskAnalyse>> findByUserUuid(String userUuid);
}