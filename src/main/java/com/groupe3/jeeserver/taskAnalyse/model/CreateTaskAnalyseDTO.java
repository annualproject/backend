package com.groupe3.jeeserver.taskAnalyse.model;

import lombok.Data;

@Data
public class CreateTaskAnalyseDTO {
    private String queryString;
    private String documentSetUuid;
    private int summarySize;
    private boolean documentSummary;
}
