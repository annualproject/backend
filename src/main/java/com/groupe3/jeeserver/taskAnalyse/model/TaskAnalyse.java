package com.groupe3.jeeserver.taskAnalyse.model;

import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.user.model.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class TaskAnalyse {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String uuid;

    @ManyToOne
    private DocumentSet documentSet;

    @Enumerated(EnumType.STRING)
    private enumeration.TaskStatus taskStatus;

    private boolean documentSummary;

    private String queryString;

    private int summarySize;

    @ManyToOne
    private User user;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @OrderBy
    private Date createdAt;

    @Type(type = "text")
    private String synthesis;

    public TaskAnalyseDTO toDTO() {
        TaskAnalyseDTO dto = new TaskAnalyseDTO();
        dto.setUuid(this.uuid);
        dto.setDocumentSetUuid(this.documentSet.getUuid());
        dto.setUserUuid(this.getUser().getUuid());
        dto.setCreatedAt(this.createdAt);
        dto.setTaskStatus(this.taskStatus);
        dto.setSynthesis(this.synthesis);
        dto.setQueryString(this.queryString);
        dto.setDocumentSummary(this.documentSummary);
        dto.setSummarySize(this.summarySize);

        return dto;
    }
}
