package com.groupe3.jeeserver.taskAnalyse.model;

import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.user.model.User;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class TaskAnalyseDTO {

    private String uuid;

    private String documentSetUuid;

    private String userUuid;

    private enumeration.TaskStatus taskStatus;

    private Date createdAt;

    private String synthesis;

    private int summarySize;

    private String queryString;

    private boolean documentSummary;
}
