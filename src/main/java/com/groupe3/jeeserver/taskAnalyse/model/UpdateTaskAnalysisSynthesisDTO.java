package com.groupe3.jeeserver.taskAnalyse.model;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class UpdateTaskAnalysisSynthesisDTO {
    private String synthesis;
}
