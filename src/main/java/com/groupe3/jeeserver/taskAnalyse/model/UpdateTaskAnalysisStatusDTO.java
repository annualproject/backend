package com.groupe3.jeeserver.taskAnalyse.model;

import lombok.Data;

@Data
public class UpdateTaskAnalysisStatusDTO {
    private int status;
}
