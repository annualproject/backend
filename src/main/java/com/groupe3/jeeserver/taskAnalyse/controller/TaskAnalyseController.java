package com.groupe3.jeeserver.taskAnalyse.controller;

import com.groupe3.jeeserver.documentSet.service.DocumentSetService;
import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.exception.TaskAnalyseNotFoundException;
import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.rabitmq.producer.model.TaskAnalyseMessage;
import com.groupe3.jeeserver.rabitmq.producer.service.RabbitmqService;
import com.groupe3.jeeserver.taskAnalyse.model.*;
import com.groupe3.jeeserver.taskAnalyse.service.TaskAnalyseService;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/taskAnalyse")
public class TaskAnalyseController {

    private final TaskAnalyseService taskAnalyseService;
    private final UserService userService;
    private final DocumentSetService documentSetService;
    private final RabbitmqService rabbitmqService;

    @Autowired
    public TaskAnalyseController(TaskAnalyseService taskAnalyseService,
                                 UserService userService,
                                 RabbitmqService rabbitmqService,
                                 DocumentSetService documentSetService) {
        this.taskAnalyseService = taskAnalyseService;
        this.userService = userService;
        this.rabbitmqService = rabbitmqService;
        this.documentSetService = documentSetService;
    }

    @PostMapping("/")
    public ResponseEntity<?> createTaskAnalyse(@RequestBody CreateTaskAnalyseDTO taskAnalyseDTO,
                                               Principal principal,
                                               UriComponentsBuilder builder) {
        TaskAnalyse taskAnalyse = this.taskAnalyseService.createTaskAnalyse(taskAnalyseDTO.getQueryString(),
                taskAnalyseDTO.isDocumentSummary(), taskAnalyseDTO.getSummarySize());

        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        this.userService.addTaskAnalysis(user.getUuid(), taskAnalyse);
        this.documentSetService.addTaskAnalyse(taskAnalyseDTO.getDocumentSetUuid(), taskAnalyse);

        this.rabbitmqService.send(new TaskAnalyseMessage(taskAnalyse.getUuid()));
        URI uri = builder.path("/taskAnalyse/{uuid}").buildAndExpand(taskAnalyse.getUuid()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @GetMapping("/{taskUuid}")
    public ResponseEntity<TaskAnalyseDTO> getTaskAnalyseByUuid(@PathVariable String taskUuid,
                                                               Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!taskAnalyseService.isUserAllowedToViewTask(user, taskUuid)) {
            return ResponseEntity.status(403).build();
        }

        TaskAnalyse taskAnalyse = taskAnalyseService.getTaskAnalyseByUuid(taskUuid)
                .orElseThrow(() -> new TaskAnalyseNotFoundException("uuid", taskUuid));
        return ok(taskAnalyse.toDTO());
    }

    @GetMapping("user/{userUuid}")
    public List<TaskAnalyseDTO> getTaskAnalyseByUser(@PathVariable String userUuid) {
        Optional<List<TaskAnalyse>> result = taskAnalyseService.getTaskAnalyseByUser(userUuid);

        if (!result.isPresent()){
            return null;
        }
        return result.get().stream().map(TaskAnalyse::toDTO).collect(Collectors.toList());
    }

    @PutMapping("/{taskUuid}/status")
    public void updateTaskAnalyseStatus(@PathVariable String taskUuid,
                                        @RequestBody UpdateTaskAnalysisStatusDTO statusDTO,
                                        Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!userService.isUserAdmin(user)) {
            ResponseEntity.status(403).build();
        }

        taskAnalyseService.updateTaskAnalyse(taskUuid, enumeration.TaskStatus.values()[statusDTO.getStatus()]);
        ResponseEntity.ok().build();
    }

    @PutMapping("/{taskUuid}/synthesis")
    public void updateTaskAnalyseSynthesis(@PathVariable String taskUuid,
                                           @RequestBody UpdateTaskAnalysisSynthesisDTO taskDTO,
                                           Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!userService.isUserAdmin(user)) {
            ResponseEntity.status(403).build();
        }

        taskAnalyseService.updateTaskAnalyseSynthesis(taskUuid, taskDTO.getSynthesis());
        ResponseEntity.ok().build();
    }
}