package com.groupe3.jeeserver.taskAnalyse.service;

import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.exception.TaskAnalyseNotFoundException;
import com.groupe3.jeeserver.exception.TaskSearchNotFoundException;
import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.taskAnalyse.repository.TaskAnalyseRepository;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.user.model.User;
import org.checkerframework.checker.nullness.Opt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class TaskAnalyseService {

    private final TaskAnalyseRepository taskAnalyseRepository;

    @Value("${admin.username}")
    private String adminUsername;


    @Autowired
    TaskAnalyseService(TaskAnalyseRepository taskAnalyseRepository){
        this.taskAnalyseRepository = taskAnalyseRepository;
    }


    public TaskAnalyse createTaskAnalyse(String queryString, boolean documentSummary, int size) {

        TaskAnalyse taskAnalyse = new TaskAnalyse();
        taskAnalyse.setQueryString(queryString);
        taskAnalyse.setDocumentSummary(documentSummary);
        taskAnalyse.setSummarySize(size);

        taskAnalyse.setTaskStatus(enumeration.TaskStatus.TO_BEGIN);
        return taskAnalyseRepository.save(taskAnalyse);
    }

    @Transactional
    public Optional<TaskAnalyse> getTaskAnalyseByUuid(String uuid) {
        return taskAnalyseRepository.findByUuid(uuid);
    }

    public Optional<List<TaskAnalyse>> getTaskAnalyseByUser(String userUuid) {
        return taskAnalyseRepository.findByUserUuid(userUuid);
    }

    public Optional<TaskAnalyse> updateTaskAnalyse(String uuid, enumeration.TaskStatus status) {
        TaskAnalyse taskAnalyse = taskAnalyseRepository.findByUuid(uuid)
                .orElseThrow(() -> new TaskAnalyseNotFoundException("uuid", uuid));


        taskAnalyse.setTaskStatus(status);

        return Optional.of(taskAnalyseRepository.save(taskAnalyse));
    }

    public Optional<TaskAnalyse> updateTaskAnalyseSynthesis(String uuid, String synthesis) {
        TaskAnalyse taskAnalyse = taskAnalyseRepository.findByUuid(uuid)
                .orElseThrow(() -> new TaskAnalyseNotFoundException("uuid", uuid));

        taskAnalyse.setSynthesis(synthesis);

        return Optional.of(taskAnalyseRepository.save(taskAnalyse));
    }

    @Transactional
    public boolean isUserAllowedToViewTask(User user, String taskUuid) {
        TaskAnalyse taskAnalyse = taskAnalyseRepository.findByUuid(taskUuid)
                .orElseThrow(() -> new TaskSearchNotFoundException("uuid", taskUuid));

        return user.getUsername().equals(this.adminUsername) || user.getUuid().equals(taskAnalyse.getUser().getUuid());
    }
}
