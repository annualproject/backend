package com.groupe3.jeeserver.user.service;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.exception.UserAlreadyExistsException;
import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.model.UserDetail;
import com.groupe3.jeeserver.user.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Value("${admin.username}")
    private String adminUsername;

    @Autowired
    UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetail loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = userRepository.findByUsername(username);
        if (optionalUser.isEmpty()) {
            throw new UsernameNotFoundException("user not found");
        }

        return new UserDetail(optionalUser.get());
    }

    public Optional<User> getUserByUuid(String uuid) {
        return userRepository.findByUuid(uuid);
    }

    public Optional<User> getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }

    public List<User> getUsers() {
        return userRepository.findAll();
    }

    public User createUser(String email, String username, String password) {

        if (userRepository.existsByUsername(username)) {
            throw new UserAlreadyExistsException(username);
        }

        User user = new User();
        user.setEmail(email);
        user.setUsername(username);
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        user.setPassword(passwordEncoder.encode(password));

        return userRepository.save(user);
    }

    public Optional<User> updateUser(String uuid, String email, Date birthday, Integer age, String phone , String name, String password){
        Optional<User> optionalUser = getUserByUuid(uuid);

        if (optionalUser.isEmpty()) {
            return Optional.empty();
        }
        User user = optionalUser.get();
        user.setEmail(email);
        user.setBirthday(birthday);
        user.setAge(age);
        user.setPhone(phone);
        user.setName(name);
        if (password != null && !password.equals("")) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            user.setPassword(passwordEncoder.encode(password));
        }
        return Optional.of(userRepository.save(user));
    }

    public void deleteUser(User user) {
        userRepository.delete(user);
    }

    @Transactional
    public void addDocumentSet(String uuid, DocumentSet documentSet) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));
        user.getDocumentSets().add(documentSet);

        documentSet.setUser(user);

        userRepository.save(user);
    }

    @Transactional
    public void removeDocumentSet(String uuid, DocumentSet documentSet) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.removeDocumentSet(documentSet);

        userRepository.save(user);
    }

    @Transactional
    public void addTaskSearch(String uuid, TaskSearch taskSearch) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.addTaskSearch(taskSearch);

        userRepository.save(user);
    }

    @Transactional
    public void removeTaskSearch(String uuid, TaskSearch taskSearch) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.removeTaskSearch(taskSearch);

        userRepository.save(user);
    }

    @Transactional
    public void addTaskAnalysis(String uuid, TaskAnalyse taskAnalyse) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.addTaskAnalysis(taskAnalyse);

        userRepository.save(user);
    }

    @Transactional
    public void removeTaskAnalysis(String uuid, TaskAnalyse taskAnalyse) {
        User user = userRepository.findByUuid(uuid).orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.removeTaskAnalysis(taskAnalyse);

        userRepository.save(user);
    }

    @Transactional
    public void addDocument(String uuid, Document document) {
        User user = userRepository.findByUuid(uuid)
                .orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.addDocument(document);

        userRepository.save(user);
    }

    @Transactional
    public void removeDocument(String uuid, Document document) {
        User user = userRepository.findByUuid(uuid)
                .orElseThrow(() -> new UserNotFoundException("uuid", uuid));

        user.removeDocument(document);

        userRepository.save(user);
    }

    public boolean isUserAdmin(User user) {
        return user.getUsername().equals(this.adminUsername);
    }
}
