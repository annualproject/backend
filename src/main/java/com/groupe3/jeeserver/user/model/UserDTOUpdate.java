package com.groupe3.jeeserver.user.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class UserDTOUpdate {
    private String uuid;
    @NotNull(message = "Please provide an email")
    @Length(min=1, message = "Please provide an email")
    @Email(message = "Please provide a valid email")
    private String email;
    private String name;
    private int age;
    private String phone;
    private Date birthday;
    @Length(min = 6, message = "Password should be at least 6 characters long")
    private String password;
}
