package com.groupe3.jeeserver.user.model;

import lombok.Data;

import java.util.Date;

@Data
public class UserInformationDTO {
    private String uuid;
    private String email;
    private String username;
    private String name;
    private int age;
    private String phone;
    private Date birthday;
}
