package com.groupe3.jeeserver.user.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class UserDTO {
    private String uuid;
    @NotNull(message = "Please provide an email")
    @Length(min=1, message = "Please provide an email")
    @Email(message = "Please provide a valid email")
    private String email;
    @NotNull(message = "Please provide a username")
    @Length(min=1, message = "Username must be at least 2 characters long")
    private String username;
    @Length(min = 6, message = "Password should be at least 6 characters long")
    @NotNull(message = "Please provide a password")
    private String password;
    private String name;
    private int age;
    private int phone;
    private Date birthday;



}
