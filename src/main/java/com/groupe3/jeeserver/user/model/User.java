package com.groupe3.jeeserver.user.model;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Getter
@Setter
@Entity(name = "AppUser")
@EntityListeners(AuditingEntityListener.class)
public class User {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String uuid;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = true)
    private String name;

    @Column(nullable = false)
    private Integer age = 0;

    @Column(nullable = true)
    private Date birthday;

    @Column(nullable = true)
    private String phone;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user"
    )
    private Set<DocumentSet> documentSets = new HashSet<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user"
    )
    private Set<TaskSearch> taskSearchs = new HashSet<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user"
    )
    private Set<TaskAnalyse> taskAnalysis = new HashSet<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "user"
    )
    private Set<Document> documents = new HashSet<>();

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public UserDTO toDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setUuid(this.uuid);
        userDTO.setEmail(this.email);
        userDTO.setUsername(this.username);
        userDTO.setPassword(this.password);
        return userDTO;
    }

    public UserDTOUpdate toDTOUpdate(){
        UserDTOUpdate userDTOupdate = new UserDTOUpdate();
        userDTOupdate.setUuid(this.uuid);
        userDTOupdate.setEmail(this.email);
        userDTOupdate.setAge(this.age);
        userDTOupdate.setBirthday(this.birthday);
        userDTOupdate.setPhone(this.phone);
        userDTOupdate.setName(this.name);
        userDTOupdate.setPassword(this.password);
        return userDTOupdate;
    }

    public UserInformationDTO userDTOinformation(){
        UserInformationDTO userinformation = new UserInformationDTO();
        userinformation.setUuid(this.uuid);
        userinformation.setUsername(this.username);
        userinformation.setEmail(this.email);
        userinformation.setAge(this.age );
        userinformation.setBirthday(this.birthday);
        userinformation.setPhone(this.phone);
        userinformation.setName(this.name);
        return userinformation;
    }

    public void addDocumentSet(DocumentSet documentSet) {
        this.documentSets.add(documentSet);
        documentSet.setUser(this);
    }

    public void removeDocumentSet(DocumentSet documentSet) {
        this.documentSets.remove(documentSet);
        documentSet.setUser(null);
    }

    public void addTaskSearch(TaskSearch taskSearch) {
        this.taskSearchs.add(taskSearch);
        taskSearch.setUser(this);
    }

    public void removeTaskSearch(TaskSearch taskSearch) {
        this.taskSearchs.remove(taskSearch);
        taskSearch.setUser(null);
    }

    public void addTaskAnalysis(TaskAnalyse taskAnalyse) {
        this.taskAnalysis.add(taskAnalyse);
        taskAnalyse.setUser(this);
    }

    public void removeTaskAnalysis(TaskAnalyse taskAnalyse) {
        this.taskAnalysis.remove(taskAnalyse);
        taskAnalyse.setUser(null);
    }

    public void addDocument(Document document) {
        this.documents.add(document);
        document.setUser(this);
    }

    public void removeDocument(Document document) {
        this.documents.remove(document);
        document.setUser(null);
    }
}
