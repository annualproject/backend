package com.groupe3.jeeserver.user.model;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Collections;

public class UserDetail implements UserDetails {

    private User user;
    public UserDetail(User user) {
        super();
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.singleton(new SimpleGrantedAuthority("USER"));
    }

    public String getUuid() { return user.getUuid(); }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }

    @Override
    public boolean isAccountNonExpired() {
        // TODO: implement later
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        // TODO: implement later
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        // TODO: implement later
        return true;
    }

    @Override
    public boolean isEnabled() {
        // TODO: implement later
        return true;
    }
}
