package com.groupe3.jeeserver.user.controller;

import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.model.UserDTO;
import com.groupe3.jeeserver.user.model.UserDTOUpdate;
import com.groupe3.jeeserver.user.model.UserInformationDTO;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.security.Principal;
import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.ResponseEntity.*;


@RestController
@RequestMapping("/user")
public class UserController {
    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/information")
    public ResponseEntity<UserInformationDTO> getInformations(Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));
        return ok(user.userDTOinformation());
    }

    @GetMapping("/{userUuid}")
    public ResponseEntity<UserInformationDTO> getUser(@PathVariable String userUuid) {
        User user = userService.getUserByUuid(userUuid)
                .orElseThrow(() -> new UserNotFoundException("uuid", userUuid));
        return ok(user.userDTOinformation());
    }

    @GetMapping
    public List<UserInformationDTO> getUsers() {
        return userService.getUsers()
                .stream()
                .map(User::userDTOinformation)
                .collect(toList());
    }

    @PostMapping
    public ResponseEntity<?> createUser(@Valid @RequestBody UserDTO userDTO, UriComponentsBuilder uriBuilder) {

        User user = userService.createUser(userDTO.getEmail(), userDTO.getUsername(), userDTO.getPassword());

        URI uri = uriBuilder.path("/user/{userUuid}").buildAndExpand(user.getUuid()).toUri();

        return created(uri).build();
    }

    @PutMapping("/{userUuid}")
    public ResponseEntity<UserInformationDTO> updateUser(@PathVariable String userUuid, @Valid @RequestBody UserDTOUpdate userDTO) {
        User user = userService.updateUser(userUuid, userDTO.getEmail(), userDTO.getBirthday(), userDTO.getAge(), userDTO.getPhone(), userDTO.getName(), userDTO.getPassword())
                .orElseThrow(() -> new UserNotFoundException("uuid", userUuid));
        return ok(user.userDTOinformation());
    }

    @DeleteMapping("/{userUuid}")
    public ResponseEntity<?> deleteUser(@PathVariable String userUuid) {
        User user = userService.getUserByUuid(userUuid)
                .orElseThrow(() -> new UserNotFoundException("uuid", userUuid));

        userService.deleteUser(user);

        return noContent().build();
    }
}
