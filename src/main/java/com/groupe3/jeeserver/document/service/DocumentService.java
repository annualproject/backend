package com.groupe3.jeeserver.document.service;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.repository.DocumentRepository;
import com.groupe3.jeeserver.exception.DocumentNotFoundException;
import com.groupe3.jeeserver.user.model.User;
import org.apache.tika.Tika;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.pdf.PDFParser;
import org.apache.tika.sax.BodyContentHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.xml.sax.SAXException;

import java.io.*;
import java.nio.file.Files;
import java.util.Optional;

@Service
public class DocumentService {

    private final DocumentRepository documentRepository;

    @Value("${uploadedDocumentsFolder}")
    private String documentsFolder;

    @Value("${admin.username}")
    private String adminUsername;

    @Autowired
    DocumentService (DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    public Document createDocument(MultipartFile file, String randomUuid) {
        String documentName = file.getOriginalFilename();

        Document document = new Document();
        document.setName(documentName);
        document.setPath(documentsFolder + "/" + randomUuid + "_" + documentName);
        document.setType(file.getContentType());

        return documentRepository.save(document);
    }

    public Document createDocument(File file) throws IOException {
        if (!file.exists()) {
            throw new FileNotFoundException();
        }

        Document document = new Document();
        document.setName(file.getName());
        document.setPath(file.getPath());
        document.setType(Files.probeContentType(file.toPath()));

        return documentRepository.save(document);
    }

    public Optional<Document> findByUuid(String uuid) {
        return this.documentRepository.findByUuid(uuid);
    }

    public String extractText(File filePath, String type) {
        Tika tika = new Tika();
        try {
            String content;

            if(type.equals("application/pdf")){
                PDFParser pdfParser = new PDFParser();
                //-1 : no size limit
                BodyContentHandler handler = new BodyContentHandler(-1);
                Metadata metaData = new Metadata();
                ParseContext parseContext = new ParseContext();
                FileInputStream fileInput = new FileInputStream(filePath);
                pdfParser.parse(fileInput, handler, metaData, parseContext);
                content = handler.toString();
            }
            else{
                content = tika.parseToString(filePath);
            }

            return content;
        }catch (IOException | TikaException | SAXException e) {
            e.printStackTrace();
            return "";
        }
    }

    @Transactional
    public boolean isUserAllowedToViewDocument(String documentUuid, User user) {
        Document document = findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        return document.getUser().getUuid().equals(user.getUuid()) ||
                user.getUsername().equals(adminUsername);
    }

    public File convertMultipartToFile(MultipartFile file, String uuid) throws IOException {
        File convFile = new File(documentsFolder + "/" + uuid + "_" + file.getOriginalFilename());
        convFile.createNewFile();

        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

    public void deleteDocument(String documentUuid) {
        Document document = findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        documentRepository.delete(document);
    }

    public void setAdminUsernameForMock(String username) {
        this.adminUsername = username;
    }
}
