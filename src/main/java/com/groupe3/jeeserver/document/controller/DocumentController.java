package com.groupe3.jeeserver.document.controller;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.service.DocumentService;
import com.groupe3.jeeserver.elasticsearch.service.ElasticsearchService;
import com.groupe3.jeeserver.exception.DocumentNotFoundException;
import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.apache.tika.io.IOUtils;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.UUID;

@RestController
@RequestMapping("/documents")
public class DocumentController {
    private final DocumentService documentService;
    private final UserService userService;
    private final ElasticsearchService elasticsearchService;

    public DocumentController(DocumentService documentService,
                              UserService userService,
                              ElasticsearchService elasticsearchService) {
        this.documentService = documentService;
        this.userService = userService;
        this.elasticsearchService = elasticsearchService;
    }

    @PostMapping
    public ResponseEntity<?> createDocument(@RequestPart MultipartFile mFile,
                                            Principal principal,
                                            UriComponentsBuilder builder) throws IOException {
        User user = this.userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));
        String randomUuid = UUID.randomUUID().toString();
        Document document = documentService.createDocument(mFile, randomUuid);
        try {
            documentService.convertMultipartToFile(mFile, randomUuid);
            userService.addDocument(user.getUuid(), document);
        } catch (IOException e) {
            e.printStackTrace();
        }

        URI uri = builder.path("/documents/{Uuid}").buildAndExpand(document.getUuid()).toUri();
        return ResponseEntity.created(uri).body("{\"documentUuid\" : \"" + document.getUuid() + "\"}");
    }

    @GetMapping("/{documentUuid}")
    public ResponseEntity<Resource> downloadDocument(@PathVariable String documentUuid,
                                                     Principal principal) throws IOException {
        User user = this.userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!documentService.isUserAllowedToViewDocument(documentUuid, user)) {
            ResponseEntity.status(403).build();
        }

        Document document = documentService.findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        File file = new File(document.getPath());
        HttpHeaders header = new HttpHeaders();
        header.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + document.getName());

        Path path = Paths.get(document.getPath());
        ByteArrayResource resource = new ByteArrayResource(Files.readAllBytes(path));

        return ResponseEntity.ok()
                .headers(header)
                .contentLength(file.length())
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(resource);
    }

    @DeleteMapping("/{documentUuid}")
    public ResponseEntity<?> deleteDocument(@PathVariable String documentUuid,
                                            Principal principal) {
        User user = this.userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!documentService.isUserAllowedToViewDocument(documentUuid, user)) {
            ResponseEntity.status(403).build();
        }

        Document document = documentService.findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        try {
            this.elasticsearchService.deleteDocument(document.getDocumentSet().getName(), document);
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }

        this.documentService.deleteDocument(documentUuid);

        return ResponseEntity.noContent().build();
    }
}
