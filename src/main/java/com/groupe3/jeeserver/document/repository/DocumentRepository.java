package com.groupe3.jeeserver.document.repository;

import com.groupe3.jeeserver.document.model.Document;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DocumentRepository extends JpaRepository<Document, String> {
    Optional<Document> findByUuid(String uuid);
}
