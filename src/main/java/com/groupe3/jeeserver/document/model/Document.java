package com.groupe3.jeeserver.document.model;

import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.user.model.User;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class Document {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String uuid;

    private String name;

    private String path;

    private String type;

    @ManyToOne
    private DocumentSet documentSet;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public DocumentDTO toDTO() {
        DocumentDTO documentDTO = new DocumentDTO();
        documentDTO.setUuid(this.getUuid());
        documentDTO.setName(this.getName());
        documentDTO.setPath(this.getPath());

        return documentDTO;
    }

    @ManyToOne
    private User user;

    @Override
    public int hashCode() {
        return getUuid().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof Document)) return false;
        Document d = (Document) obj;
        return d.getUuid().equals(this.getUuid());
    }
}
