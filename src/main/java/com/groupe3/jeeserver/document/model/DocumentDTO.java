package com.groupe3.jeeserver.document.model;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.File;

@Data
public class DocumentDTO {
    private String uuid;
    @NotNull(message = "Please provide a file")
    private File file;
    private String name;
    private String path;
}
