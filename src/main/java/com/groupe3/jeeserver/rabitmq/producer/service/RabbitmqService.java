package com.groupe3.jeeserver.rabitmq.producer.service;


import com.groupe3.jeeserver.rabitmq.producer.model.TaskAnalyseMessage;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;


@Service
public class RabbitmqService {
    private final RabbitTemplate rabbitTemplate;

    @Value("${spring.rabbitmq.queue-name}")
    private String queueName;

    @Autowired
    public RabbitmqService(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void send(TaskAnalyseMessage message) {
        rabbitTemplate.convertAndSend(queueName, message);
    }
}
