package com.groupe3.jeeserver.rabitmq.producer.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@AllArgsConstructor
public class TaskAnalyseMessage implements Serializable {
    String taskUuid;
}
