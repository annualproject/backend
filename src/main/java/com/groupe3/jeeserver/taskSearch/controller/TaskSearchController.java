package com.groupe3.jeeserver.taskSearch.controller;

import com.groupe3.jeeserver.exception.TaskSearchNotFoundException;
import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.taskSearch.model.TaskSearchDTO;
import com.groupe3.jeeserver.taskSearch.service.TaskSearchService;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@RequestMapping("/taskSearch")
public class TaskSearchController {

    private final TaskSearchService taskSearchService;
    private final UserService userService;

    @Autowired
    public TaskSearchController(TaskSearchService taskSearchService, UserService userService) {
        this.taskSearchService = taskSearchService;
        this.userService = userService;
    }

    @GetMapping("/{taskUuid}")
    public ResponseEntity<TaskSearchDTO> getTaskSearchByUuid(@PathVariable String taskUuid,
                                                             Principal principal) {
        User user = this.userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        if (!taskSearchService.isUserAllowedToViewTask(user, taskUuid)) {
            return ResponseEntity.status(403).build();
        }

        TaskSearch taskSearch = taskSearchService.getTaskSearchByUuid(taskUuid)
                .orElseThrow(() -> new TaskSearchNotFoundException("uuid", taskUuid));
        return ok(taskSearch.toDTO());
    }

    @GetMapping("user/{userUuid}")
    public List<TaskSearchDTO> getTaskSearchByUser(@PathVariable String userUuid) {
        Optional<List<TaskSearch>> result = taskSearchService.getTaskSearchByUser(userUuid);

        if (!result.isPresent()){
            return null;
        }
        return result.get().stream().map(TaskSearch::toDTO).collect(Collectors.toList());
    }
}
