package com.groupe3.jeeserver.taskSearch.repository;

import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface TaskSearchRepository extends JpaRepository<TaskSearch, Long> {
    Optional<TaskSearch> findByUuid(String uuid);
    Optional<List<TaskSearch>> findByUserUuid(String userUuid);
}