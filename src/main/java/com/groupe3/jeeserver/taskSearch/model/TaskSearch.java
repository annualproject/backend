package com.groupe3.jeeserver.taskSearch.model;

import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.user.model.User;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class TaskSearch {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String uuid;

    @Column(nullable = false)
    private String searchQuery;

    @Enumerated(EnumType.STRING)
    private enumeration.TaskStatus taskStatus;

    @ManyToOne
    private User user;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;


    public TaskSearchDTO toDTO() {
        TaskSearchDTO dto = new TaskSearchDTO();
        dto.setUuid(this.uuid);
        dto.setSearchQuery(this.searchQuery);
        dto.setUserUuid(this.getUser().getUuid());
        dto.setCreatedAt(this.createdAt);
        dto.setTaskStatus(this.taskStatus);

        return dto;
    }
}
