package com.groupe3.jeeserver.taskSearch.model;

import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.user.model.User;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import java.util.Date;

@Data
public class TaskSearchDTO {

    private String uuid;

    @NotEmpty
    private String searchQuery;

    private String userUuid;

    private enumeration.TaskStatus taskStatus;

    private Date createdAt;

}
