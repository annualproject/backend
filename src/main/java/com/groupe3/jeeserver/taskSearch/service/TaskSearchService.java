package com.groupe3.jeeserver.taskSearch.service;

import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.exception.TaskSearchInProgressException;
import com.groupe3.jeeserver.exception.TaskSearchNotFoundException;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.taskSearch.model.TaskSearchDTO;
import com.groupe3.jeeserver.taskSearch.repository.TaskSearchRepository;
import com.groupe3.jeeserver.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskSearchService {

    private final TaskSearchRepository taskSearchRepository;

    @Value("${admin.username}")
    private String adminUsername;

    @Autowired
    TaskSearchService(TaskSearchRepository taskSearchRepository){
        this.taskSearchRepository = taskSearchRepository;
    }

    public TaskSearch createTaskSearch(String searchQuery, String userUid) {
        TaskSearch taskSearch = new TaskSearch();
        Optional<List<TaskSearch>> userTaskSearches = getTaskSearchByUser(userUid);

        if(userTaskSearches.isPresent()){
            List<TaskSearchDTO> listUserTaskSearches = userTaskSearches.get().stream().map(TaskSearch::toDTO).collect(Collectors.toList());
            for (TaskSearchDTO task : listUserTaskSearches) {

                if (task.getSearchQuery().equals(searchQuery)&& task.getTaskStatus() != enumeration.TaskStatus.ERROR
                        && task.getTaskStatus() != enumeration.TaskStatus.FINISHED) {
                    throw new TaskSearchInProgressException(searchQuery);
                }
            }
        }
        taskSearch.setSearchQuery(searchQuery);
        taskSearch.setTaskStatus(enumeration.TaskStatus.TO_BEGIN);
        return taskSearchRepository.save(taskSearch);
    }

    @Transactional
    public Optional<TaskSearch> getTaskSearchByUuid(String uuid) {
        return taskSearchRepository.findByUuid(uuid);
    }

    public Optional<List<TaskSearch>> getTaskSearchByUser(String userUuid) {
        return taskSearchRepository.findByUserUuid(userUuid);
    }

    @Transactional
    public boolean isUserAllowedToViewTask(User user, String taskUuid) {
        TaskSearch taskSearch = taskSearchRepository.findByUuid(taskUuid)
                .orElseThrow(() -> new TaskSearchNotFoundException("uuid", taskUuid));

        return user.getUsername().equals(this.adminUsername) || user.getUuid().equals(taskSearch.getUser().getUuid());
    }

    public Optional<TaskSearch> updateTaskSearch(String uuid, enumeration.TaskStatus status) {
        Optional<TaskSearch> optionalTaskSearch = getTaskSearchByUuid(uuid);
        if (optionalTaskSearch.isEmpty()) {
            return Optional.empty();
        }

        TaskSearch taskSearch = optionalTaskSearch.get();
        taskSearch.setTaskStatus(status);
        return Optional.of(taskSearchRepository.save(taskSearch));
    }
}
