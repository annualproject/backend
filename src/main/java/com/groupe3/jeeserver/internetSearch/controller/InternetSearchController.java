package com.groupe3.jeeserver.internetSearch.controller;

import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.internetSearch.model.InternetSearchDTO;
import com.groupe3.jeeserver.internetSearch.service.InternetSearchService;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.taskSearch.service.TaskSearchService;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping("/search")
public class InternetSearchController {
    private final InternetSearchService internetSearchService;
    private final UserService userService;
    private final TaskSearchService taskSearchService;

    @Autowired
    public InternetSearchController(InternetSearchService internetSearchService, UserService userService, TaskSearchService taskSearchService) {
        this.internetSearchService = internetSearchService;
        this.userService = userService;
        this.taskSearchService = taskSearchService;
    }

    @PostMapping
    public ResponseEntity<?> search(@Valid @RequestBody InternetSearchDTO internetSearchDTO, Principal principal) {

        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        TaskSearch taskSearch = taskSearchService.createTaskSearch(internetSearchDTO.getSearchQuery(), user.getUuid());
        userService.addTaskSearch(user.getUuid(), taskSearch);

        // Asynchronous task
        boolean searchInProgress = this.internetSearchService.asynchronousSearchAndDownload(
                internetSearchDTO.getSearchQuery(), user, taskSearch
        );

        if(searchInProgress){
            return ResponseEntity.ok().build();
        }
        else{
            return ResponseEntity.badRequest().build();
        }
    }

}
