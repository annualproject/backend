package com.groupe3.jeeserver.internetSearch.model;

import lombok.Data;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
public class InternetSearchDTO {
    @NotNull(message = "Please provide a search query")
    @Length(min=1, message = "Please don't provide a empty search query")
    private String searchQuery;
}
