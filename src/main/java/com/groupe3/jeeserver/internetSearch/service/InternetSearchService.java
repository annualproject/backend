package com.groupe3.jeeserver.internetSearch.service;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.customsearch.Customsearch;
import com.google.api.services.customsearch.CustomsearchRequestInitializer;
import com.google.api.services.customsearch.model.Result;
import com.google.api.services.customsearch.model.Search;
import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.service.DocumentService;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.documentSet.repository.DocumentSetRepository;
import com.groupe3.jeeserver.documentSet.service.DocumentSetService;
import com.groupe3.jeeserver.elasticsearch.service.ElasticsearchService;
import com.groupe3.jeeserver.enumeration.enumeration;
import com.groupe3.jeeserver.taskSearch.model.TaskSearch;
import com.groupe3.jeeserver.taskSearch.service.TaskSearchService;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

@Service
public class InternetSearchService {

    @Value("${downloadedDocumentsFolder}")
    private String downloadedDocumentsFolder;

    private final DocumentSetService documentSetService;
    private final DocumentService documentService;
    private final UserService userService;
    private final ElasticsearchService elasticsearchService;
    private final DocumentSetRepository documentSetRepository;
    private final TaskSearchService taskSearchService;
    private final String searchEngineKey;
    private final String googleClientKey;

    @Autowired
    public InternetSearchService(DocumentSetService documentSetService, UserService userService,
                                 ElasticsearchService elasticsearchService, DocumentService documentService,
                                 DocumentSetRepository documentSetRepository,
                                 TaskSearchService taskSearchService,
                                 @Value("${google.search.engine.key}") String searchEngineKey,
                                 @Value("${google.client.key}") String googleClientKey) {
        this.documentSetService = documentSetService;
        this.userService = userService;
        this.elasticsearchService = elasticsearchService;
        this.documentService = documentService;
        this.documentSetRepository = documentSetRepository;
        this.taskSearchService = taskSearchService;
        this.searchEngineKey = searchEngineKey;
        this.googleClientKey = googleClientKey;
    }

    public Hashtable<String, String> googleSearchDoc(String searchQuery) throws GeneralSecurityException, IOException {
        System.out.println(searchQuery);

        String cx = searchEngineKey; //search engine google private

        Customsearch cs = new Customsearch.Builder(GoogleNetHttpTransport.newTrustedTransport(),
                JacksonFactory.getDefaultInstance(),
                null)
                    .setApplicationName("PA project")
                    .setGoogleClientRequestInitializer(
                            new CustomsearchRequestInitializer(googleClientKey)
                    )
                    .build();

        Customsearch.Cse.List list = cs.cse().list(searchQuery).setCx(cx).setFileType("pdf").setLr("lang_fr");

        Search result = list.execute();
        Hashtable<String, String> docsUrl= new Hashtable<>();
        int i = 0;
        if (result.getItems()!=null){
            for (Result ri : result.getItems()) {
                if(i == 19){
                    break;
                }
                docsUrl.put("document_" + i, ri.getLink());
                System.out.println("File downloaded");
                System.out.println("document_" + i);
                i++;
            }
        }
        return docsUrl;
    }

    public void downloadDoc(String docTitle, String docUrl, String docFolder) {
        try{
            URL url = new URL(docUrl);

            InputStream in = url.openStream();

            new File(docFolder).mkdir();

            Path filePath = Paths.get(docFolder + File.separator + docTitle + ".pdf");

            System.out.println(filePath.toString());
            FileOutputStream fos = new FileOutputStream(new File( filePath.toString()));

            int length = -1;
            byte[] buffer = new byte[1024];
            while ((length = in.read(buffer)) > -1) {
                fos.write(buffer, 0, length);
            }
            fos.close();
            in.close();
        }
        catch (Exception e){
            System.out.println("Erreur");
        }

    }

    public void saveDocumentsAndUploadToElasticSearch(String searchQuery, User user, String docFolder){

        String documentSetName = documentSetService.convertToElasticSearchCase(
                            user.getUsername() + "_" + searchQuery
                                );

        Optional<DocumentSet> documentSetOptional = this.documentSetService.findByName(documentSetName);
        DocumentSet documentSet;


        if (documentSetOptional.isEmpty()) {
            documentSet = documentSetService.create(documentSetName);
            userService.addDocumentSet(user.getUuid(), documentSet);
        } else {
            documentSet = documentSetOptional.get();
        }

        try {
            //delete existing index to create a empty one and be able to make a search several times
            elasticsearchService.deleteIndex(documentSetName);
            elasticsearchService.createIndexIfNotExists(documentSetName);
        } catch (IOException e) {
            e.printStackTrace();
        }

        File folder = new File(docFolder);
        String[] fileList = folder.list();

        if (fileList != null) {
            for (int i = 0; i < fileList.length; i++) {
                Document file;
                try {
                    String path = docFolder + File.separator + fileList[i];
                    file = documentService.createDocument(
                            new File(path)
                    );
                    userService.addDocument(user.getUuid(), file);
                    documentSetService.addDocument(documentSet.getUuid(), file);
                    elasticsearchService.indexDocument(documentSetName, file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        } else {
            System.err.println("Error");
        }
    }

    public boolean asynchronousSearchAndDownload(String searchQuery, User user, TaskSearch taskSearch){
        ExecutorService threadpool = Executors.newCachedThreadPool();
        Future futureTask = threadpool.submit((Runnable) () -> {

            //Search pdf documents on google
            Hashtable<String, String> docsUrl = null;
            try {
                docsUrl = googleSearchDoc(searchQuery);
            } catch (GeneralSecurityException | IOException e) {
                e.printStackTrace();
            }

            if(docsUrl != null) {

                String docFolder = downloadedDocumentsFolder + "_" + UUID.randomUUID();

                //Download found documents (max 10)
                for (Map.Entry<String, String> entry : docsUrl.entrySet()) {
                    String title = entry.getKey();
                    String url = entry.getValue();

                    downloadDoc(title, url, docFolder);
                }

                //save and upload text to elasticSearch
                saveDocumentsAndUploadToElasticSearch(searchQuery, user, docFolder);

                //TODO call the search engine
            }

            taskSearchService.updateTaskSearch(taskSearch.getUuid(), enumeration.TaskStatus.FINISHED);
        });

        if(!futureTask.isDone()){
            taskSearchService.updateTaskSearch(taskSearch.getUuid(), enumeration.TaskStatus.IN_PROGRESS);
            return true;
        }
        threadpool.shutdown();
        return true;
    }
}
