package com.groupe3.jeeserver;

import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.Optional;

@Component
public class InitDatabase implements CommandLineRunner {
    private final UserService userService;

    @Value("${admin.username}")
    private String adminUsername;

    @Value("${admin.password}")
    private String adminPassword;

    @Value("${uploadedDocumentsFolder}")
    private String documentsFolder;

    @Autowired
    public InitDatabase(UserService userService) {
        this.userService = userService;
    }

    @Override
    public void run(String... args) throws Exception {
        Optional<User> optionalUser = userService.getUserByUsername(adminUsername);

        if (optionalUser.isEmpty()) {
            this.userService.createUser("admin@admin.com", adminUsername, adminPassword);
        }

        File documentFolder = new File(this.documentsFolder);
        if (!documentFolder.exists()) {
            if (!documentFolder.mkdir()) {
                System.err.println("Error while creating the documents folder");
            }
        }
    }
}
