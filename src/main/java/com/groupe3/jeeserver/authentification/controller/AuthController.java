package com.groupe3.jeeserver.authentification.controller;

import com.groupe3.jeeserver.authentification.model.AuthRequest;
import com.groupe3.jeeserver.authentification.model.AuthResponse;
import com.groupe3.jeeserver.authentification.util.JwtUtil;
import com.groupe3.jeeserver.user.model.UserDetail;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;
    private UserService userService;
    private JwtUtil jwtUtil;

    @Autowired
    AuthController(AuthenticationManager authenticationManager, UserService userService, JwtUtil jwtUtil){
        this.authenticationManager = authenticationManager;
        this.userService = userService;
        this.jwtUtil = jwtUtil;
    };

    @PostMapping
    public ResponseEntity<?> createAuth(@Valid @RequestBody AuthRequest authRequest) throws Exception {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword()));
        }catch (BadCredentialsException e){
            throw new Exception("Incorrect username or password", e);
        }
        final UserDetail userDetail = userService.loadUserByUsername(authRequest.getUsername());

        final String jwt = jwtUtil.generateToken(userDetail);

        return ResponseEntity.ok(new AuthResponse(jwt));

    }
}
