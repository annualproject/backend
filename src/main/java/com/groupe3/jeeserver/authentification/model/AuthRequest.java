package com.groupe3.jeeserver.authentification.model;
import lombok.Data;

@Data
public class AuthRequest {
    private String username;
    private String password;
}
