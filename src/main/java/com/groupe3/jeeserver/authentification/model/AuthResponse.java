package com.groupe3.jeeserver.authentification.model;

import lombok.Data;

@Data
public class AuthResponse {
    private final String jwt;
}
