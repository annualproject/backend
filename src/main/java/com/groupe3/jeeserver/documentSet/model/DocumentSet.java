package com.groupe3.jeeserver.documentSet.model;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.user.model.User;
import lombok.Getter;
import lombok.Setter;
import org.elasticsearch.tasks.Task;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@EntityListeners(AuditingEntityListener.class)
public class DocumentSet {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    @Column(updatable = false, nullable = false)
    private String uuid;

    @Column(unique = true)
    private String name;

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "documentSet"
    )
    private Set<Document> documents = new HashSet<>();

    @OneToMany(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "documentSet"
    )
    private Set<TaskAnalyse> taskAnalyseSet = new HashSet<>();

    @ManyToOne
    private User user;

    @Column(nullable = false, updatable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date createdAt;

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    private Date updatedAt;

    public void addDocument(Document document) {
        this.documents.add(document);
        document.setDocumentSet(this);
    }

    public void removeDocument(Document document) {
        this.getDocuments().remove(document);
        document.setDocumentSet(null);
    }

    public void addTaskAnalyse(TaskAnalyse taskAnalyse) {
        this.getTaskAnalyseSet().add(taskAnalyse);
        taskAnalyse.setDocumentSet(this);
    }

    public void removeTaskAnalyse(TaskAnalyse taskAnalyse) {
        this.getTaskAnalyseSet().remove(taskAnalyse);
        taskAnalyse.setDocumentSet(null);
    }

    public DocumentSetDTO toDTO() {
        DocumentSetDTO dto = new DocumentSetDTO();
        dto.setName(this.name);
        dto.setUuid(this.uuid);

        return dto;
    }

    @Override
    public int hashCode() {
        return getUuid().hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (!(obj instanceof DocumentSet)) return false;
        DocumentSet d = (DocumentSet) obj;
        return d.getUuid().equals(this.getUuid());
    }
}
