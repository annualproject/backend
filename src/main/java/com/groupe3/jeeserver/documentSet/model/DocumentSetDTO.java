package com.groupe3.jeeserver.documentSet.model;

import lombok.Data;

import javax.validation.constraints.NotEmpty;

@Data
public class DocumentSetDTO {

    private String uuid;

    @NotEmpty
    private String name;
}
