package com.groupe3.jeeserver.documentSet.controller;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.model.DocumentDTO;
import com.groupe3.jeeserver.document.service.DocumentService;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.documentSet.model.DocumentSetDTO;
import com.groupe3.jeeserver.documentSet.service.DocumentSetService;
import com.groupe3.jeeserver.elasticsearch.service.ElasticsearchService;
import com.groupe3.jeeserver.exception.DocumentNotFoundException;
import com.groupe3.jeeserver.exception.DocumentSetNotFoundException;
import com.groupe3.jeeserver.exception.UserNotFoundException;
import com.groupe3.jeeserver.user.model.User;
import com.groupe3.jeeserver.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.io.IOException;
import java.net.URI;
import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@RestController
@RequestMapping("/documentSets")
public class DocumentSetController {
    private final DocumentSetService documentSetService;
    private final UserService userService;
    private final DocumentService documentService;
    private final ElasticsearchService elasticsearchService;

    @Autowired
    public DocumentSetController(DocumentSetService service,
                                 UserService userService,
                                 DocumentService documentService,
                                 ElasticsearchService elasticsearchService) {
        this.documentSetService = service;
        this.userService = userService;
        this.documentService = documentService;
        this.elasticsearchService = elasticsearchService;
    }



    @GetMapping
    public List<DocumentSetDTO> getAll() {
        return this.documentSetService.getAll()
                .stream()
                .map(DocumentSet::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/userSets")
    public List<DocumentSetDTO> getAllByUser(Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        Optional<List<DocumentSet>> result = this.documentSetService.getAllByUser(user.getUsername());

        if (!result.isPresent()){
            return null;
        }

        return result.get().stream().map(DocumentSet::toDTO).collect(Collectors.toList());
    }

    @GetMapping("/{setUuid}")
    public ResponseEntity<DocumentSetDTO> getOne(@PathVariable String setUuid,
                                 Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        DocumentSet documentSet = documentSetService.findByUuid(setUuid)
                .orElseThrow(() -> new DocumentSetNotFoundException("uuid", setUuid));

        if (!documentSetService.checkDocumentSetOwner(documentSet, user)) {
            return ResponseEntity.status(403).build();
        }

        return ResponseEntity.ok(documentSet.toDTO());
    }

    @GetMapping("/{setUuid}/documents")
    public ResponseEntity<List<DocumentDTO>> getDocumentsFromSet(@PathVariable String setUuid,
                                                                 Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        DocumentSet documentSet = documentSetService.findByUuid(setUuid)
                .orElseThrow(() -> new DocumentSetNotFoundException("uuid", setUuid));

        if (!documentSetService.checkDocumentSetOwner(documentSet, user)) {
            return ResponseEntity.status(403).build();
        }

        return ResponseEntity.ok(this.documentSetService.getDocuments(setUuid));
    }

    @PostMapping
    public ResponseEntity<?> create(@Valid @RequestBody DocumentSetDTO documentSetDTO,
                                    UriComponentsBuilder uriBuilder,
                                    Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        String documentSetName = user.getUsername() + "_" + documentSetDTO.getName();
        DocumentSet documentSet = documentSetService.create(documentSetName);
        userService.addDocumentSet(user.getUuid(), documentSet);

        try {
            elasticsearchService.createIndexIfNotExists(documentSet.getName());
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().build();
        }

        URI uri = uriBuilder.path("/documentSets/{uuid}").buildAndExpand(documentSet.getUuid()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @PutMapping("/{setUuid}/documents/{documentUuid}")
    public ResponseEntity<?> addDocument(@PathVariable String setUuid,
                                         @PathVariable String documentUuid,
                                         Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        DocumentSet documentSet = documentSetService.findByUuid(setUuid)
                .orElseThrow(() -> new DocumentSetNotFoundException("uuid", setUuid));

        if (!documentSetService.checkDocumentSetOwner(documentSet, user)) {
            return ResponseEntity.status(403).build();
        }

        if (!documentService.isUserAllowedToViewDocument(documentUuid, user)) {
            return ResponseEntity.status(403).build();
        }

        Document document = documentService.findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        documentSetService.addDocument(setUuid, document);

        try {
            this.elasticsearchService.indexDocument(documentSet.getName(), document);
        } catch (IOException e) {
            documentSetService.removeDocument(setUuid, document);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }

    @DeleteMapping("/{setUuid}/documents/{documentUuid}")
    public ResponseEntity<?> removeDocument(@PathVariable String setUuid,
                                         @PathVariable String documentUuid,
                                         Principal principal) {
        User user = userService.getUserByUsername(principal.getName())
                .orElseThrow(() -> new UserNotFoundException("username", principal.getName()));

        DocumentSet documentSet = documentSetService.findByUuid(setUuid)
                .orElseThrow(() -> new DocumentSetNotFoundException("uuid", setUuid));

        if (!documentSetService.checkDocumentSetOwner(documentSet, user)) {
            return ResponseEntity.status(403).build();
        }

        Document document = documentService.findByUuid(documentUuid)
                .orElseThrow(() -> new DocumentNotFoundException("uuid", documentUuid));

        documentSetService.removeDocument(setUuid, document);

        try {
            elasticsearchService.deleteDocument(documentSet.getName(), document);
        } catch (IOException e) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.noContent().build();
    }
}
