package com.groupe3.jeeserver.documentSet.repository;

import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface DocumentSetRepository extends JpaRepository<DocumentSet, String> {
    Optional<DocumentSet> findByUuid(String uuid);
    boolean existsByName(String name);
    Optional<List<DocumentSet>> findByNameStartsWith(String username);
    Optional<DocumentSet> findByName(String name);
}
