package com.groupe3.jeeserver.documentSet.service;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.model.DocumentDTO;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.documentSet.repository.DocumentSetRepository;
import com.groupe3.jeeserver.exception.DocumentSetAlreadyExistsException;
import com.groupe3.jeeserver.exception.DocumentSetNotFoundException;
import com.groupe3.jeeserver.taskAnalyse.model.TaskAnalyse;
import com.groupe3.jeeserver.user.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DocumentSetService {
    private final DocumentSetRepository repository;

    @Value("${admin.username}")
    private String adminUsername;

    @Autowired
    public DocumentSetService(DocumentSetRepository repository) {
        this.repository = repository;
    }

    public List<DocumentSet> getAll() {
        return repository.findAll();
    }

    public DocumentSet create(String name) {

        name = convertToElasticSearchCase(name);
        DocumentSet documentSet = new DocumentSet();

        if(repository.existsByName(name)){
            throw new DocumentSetAlreadyExistsException(name);
        }

        documentSet.setName(name);

        return repository.save(documentSet);
    }


    public Optional<List<DocumentSet>> getAllByUser(String username){
        username = convertToElasticSearchCase(username);
        return repository.findByNameStartsWith(username);
    }

    public Optional<DocumentSet> findByUuid(String uuid) {
        return this.repository.findByUuid(uuid);
    }

    public Optional<DocumentSet> findByName(String name) {
        return repository.findByName(name);
    }

    @Transactional
    public void addDocument(String uuid, Document document) {
        DocumentSet documentSet = repository.findByUuid(uuid)
                .orElseThrow(() ->
                        new DocumentSetNotFoundException("uuid", uuid));

        documentSet.addDocument(document);
        repository.save(documentSet);
    }

    @Transactional
    public void removeDocument(String uuid, Document document) {
        DocumentSet documentSet = repository.findByUuid(uuid)
                .orElseThrow(() ->
                        new DocumentSetNotFoundException("uuid", uuid));

        documentSet.removeDocument(document);

        repository.save(documentSet);
    }

    public boolean checkDocumentSetOwner(DocumentSet documentSet, User user) {
        return documentSet.getUser().getUuid().equals(user.getUuid()) ||
                user.getUsername().equals(adminUsername);
    }

    @Transactional
    public List<DocumentDTO> getDocuments(String documentSetUuid) {
        DocumentSet documentSet = repository.findByUuid(documentSetUuid)
                .orElseThrow(() -> new DocumentSetNotFoundException("uuid", documentSetUuid));

        return documentSet.getDocuments()
                .stream()
                .map(Document::toDTO)
                .collect(Collectors.toList());
    }

    @Transactional
    public void addTaskAnalyse(String uuid, TaskAnalyse taskAnalyse) {
        DocumentSet documentSet = repository.findByUuid(uuid)
                .orElseThrow(() ->
                        new DocumentSetNotFoundException("uuid", uuid));
        documentSet.addTaskAnalyse(taskAnalyse);

        repository.save(documentSet);
    }

    @Transactional
    public void removeTaskAnalyse(String uuid, TaskAnalyse taskAnalyse) {
        DocumentSet documentSet = repository.findByUuid(uuid)
                .orElseThrow(() ->
                        new DocumentSetNotFoundException("uuid", uuid));

        documentSet.removeTaskAnalyse(taskAnalyse);

        repository.save(documentSet);
    }

    public String convertToElasticSearchCase(String datasetName){
        datasetName = datasetName.toLowerCase();
        //inférieur a 255 char

        datasetName = datasetName.replaceAll("\\s|[\\\\]|[/]|[*]|[?]|[\"]|[<]|[>]|[|]|[,]|[#]|[:]", "_");
        return datasetName.replaceAll("^([-]|[_]|[+]|[.])", "x");

    }

    public void setAdminUsernameForMock(String username) {
        this.adminUsername = username;
    }
}