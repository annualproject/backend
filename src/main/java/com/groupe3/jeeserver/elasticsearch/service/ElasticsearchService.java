package com.groupe3.jeeserver.elasticsearch.service;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.service.DocumentService;
import org.apache.http.HttpHost;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Service
public class ElasticsearchService {
    private final RestHighLevelClient client;
    private final DocumentService documentService;

    @Autowired
    public ElasticsearchService(@Value("${elasticsearch.host}") String url,
                                @Value("${elasticsearch.port}") int port,
                                @Value("${elasticsearch.protocol}") String protocol,
                                @Value("${elasticsearch.username}") String username,
                                @Value("${elasticsearch.password}") String password,
                                DocumentService documentService) {
        if (username != null && password != null) {
            this.client = this.createClientWithAuth(url, port, protocol, username, password);
        } else {
            this.client = this.createClientWithoutAuth(url, port, protocol);
        }

        this.documentService = documentService;
    }

    private RestHighLevelClient createClientWithoutAuth(String url, int port, String protocol) {
        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(url, port, protocol)
                )
        );
    }

    private RestHighLevelClient createClientWithAuth(String url,
                                                     int port,
                                                     String protocol,
                                                     String username,
                                                     String password) {
        CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
        credentialsProvider.setCredentials(
                AuthScope.ANY,
                new UsernamePasswordCredentials(username, password)
        );

        return new RestHighLevelClient(
                RestClient.builder(
                        new HttpHost(url, port, protocol)
                ).setHttpClientConfigCallback(httpAsyncClientBuilder ->
                        httpAsyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
                )
        );
    }

    public void close() throws IOException {
        this.client.close();
    }

    public void createIndexIfNotExists(String name) throws IOException {
        GetIndexRequest existsRequest = new GetIndexRequest(name);

        if (client.indices().exists(existsRequest, RequestOptions.DEFAULT)) {
            return;
        }

        CreateIndexRequest createRequest = new CreateIndexRequest(name);

        // This is a synchronous request, it will wait for index creation to continue.
        client.indices().create(createRequest, RequestOptions.DEFAULT);
    }

    public void deleteIndex(String name) throws IOException {
        GetIndexRequest existsRequest = new GetIndexRequest(name);

        if (!client.indices().exists(existsRequest, RequestOptions.DEFAULT)) {
            return;
        }

        DeleteIndexRequest request = new DeleteIndexRequest(name);
        client.indices().delete(request, RequestOptions.DEFAULT);
    }

    public void indexDocument(String indexName, Document document) throws IOException {
        Map<String, Object> jsonMap = new HashMap<>();

        String content = documentService.extractText(
                new File(document.getPath()),
                document.getType()
        );

        jsonMap.put("content", content);
        // Add extra fields there.

        IndexRequest request = new IndexRequest(indexName)
                .id(document.getUuid())
                .source(jsonMap);

        client.index(request, RequestOptions.DEFAULT);
    }

    public void deleteDocument(String indexName, Document document) throws IOException {
        DeleteRequest request = new DeleteRequest(indexName)
                .id(document.getUuid());

        client.delete(request, RequestOptions.DEFAULT);
    }
}
