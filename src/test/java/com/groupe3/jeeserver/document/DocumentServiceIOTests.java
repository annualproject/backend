package com.groupe3.jeeserver.document;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.repository.DocumentRepository;
import com.groupe3.jeeserver.document.service.DocumentService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceIOTests {
    @Mock
    private DocumentRepository documentRepository;

    @InjectMocks
    private DocumentService documentService;

    private File testFile;
    private String testFileType;
    private String testFileContent;

    @Before
    public void init() throws IOException {
        when(documentRepository.save(any(Document.class))).thenAnswer(fx -> {
            Document d = (Document) fx.getArguments()[0];
            d.setUuid("uuid");
            return d;
        });

        testFile = new File("test.txt");
        testFileType = "text/plain";
        testFileContent = "This is a test file.";

        assertTrue(testFile.createNewFile());

        FileWriter writer = new FileWriter(testFile);
        BufferedWriter bufferedWriter = new BufferedWriter(writer);

        bufferedWriter.write(testFileContent);
        bufferedWriter.close();
    }

    @After
    public void clean() {
        assertTrue(testFile.delete());
    }

    @Test
    public void create_document_from_file_should_work() throws IOException {
        Document createdDocument = documentService.createDocument(testFile);

        verify(documentRepository, times(1)).save(any(Document.class));
        assertEquals("uuid", createdDocument.getUuid());
        assertEquals("text/plain", createdDocument.getType());
    }

    @Test(expected = FileNotFoundException.class)
    public void created_document_without_file_should_throw_an_exception() throws IOException {
        File wrongFile = new File("wrongPath.txt");

        documentService.createDocument(wrongFile);
    }

    @Test
    public void parse_content_of_text_file_should_return_the_content() {
        String content = documentService.extractText(testFile, testFileType);

        assertEquals(testFileContent, content.strip());
    }

    @Test
    public void parse_content_without_file_should_return_empty_string() {
        File wrongFile = new File("wrongPath.txt");

        String content = documentService.extractText(wrongFile, testFileType);

        assertEquals("", content.strip());
    }
}
