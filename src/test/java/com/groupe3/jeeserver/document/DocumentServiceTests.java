package com.groupe3.jeeserver.document;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.document.repository.DocumentRepository;
import com.groupe3.jeeserver.document.service.DocumentService;
import com.groupe3.jeeserver.user.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.test.context.event.annotation.BeforeTestClass;
import org.springframework.test.context.event.annotation.BeforeTestMethod;

import java.io.*;
import java.nio.file.Files;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DocumentServiceTests {
    @Mock
    private DocumentRepository documentRepository;

    @InjectMocks
    private DocumentService documentService;

    private User admin;
    private User normalUser;
    private User normalUser2;

    @Before
    public void setup() {
        admin = new User();
        normalUser = new User();
        normalUser2 = new User();

        documentService.setAdminUsernameForMock("admin");

        admin.setUsername("admin");
        admin.setUuid("1");

        normalUser.setUsername("Poline");
        normalUser.setUuid("2");

        normalUser2.setUsername("Hourdïa");
        normalUser2.setUuid("3");

        Document document = new Document();
        document.setUuid("98dc57b3-7a06-4f19-8d4c-0932e9651610");
        document.setUser(normalUser);

        when(documentRepository.findByUuid("98dc57b3-7a06-4f19-8d4c-0932e9651610"))
                .thenReturn(Optional.of(document));
    }

    @Test
    public void findByUuid_should_return_a_document() {
        Optional<Document> document = documentService.findByUuid("98dc57b3-7a06-4f19-8d4c-0932e9651610");

        assertTrue(document.isPresent());
        assertEquals("98dc57b3-7a06-4f19-8d4c-0932e9651610", document.get().getUuid());
        verify(documentRepository, times(1)).findByUuid("98dc57b3-7a06-4f19-8d4c-0932e9651610");
    }

    @Test
    public void findByUuid_with_wrong_uuid_should_return_empty_optional() {
        Optional<Document> document = documentService.findByUuid("lol");

        assertTrue(document.isEmpty());
        verify(documentRepository, times(1)).findByUuid("lol");
    }

    @Test
    public void delete_should_call_delete_method_in_repository() {
        ArgumentCaptor<Document> documentArgumentCaptor = ArgumentCaptor.forClass(Document.class);
        doNothing().when(documentRepository).delete(documentArgumentCaptor.capture());

        documentService.deleteDocument("98dc57b3-7a06-4f19-8d4c-0932e9651610");

        assertEquals("98dc57b3-7a06-4f19-8d4c-0932e9651610", documentArgumentCaptor.getValue().getUuid());
        verify(documentRepository, times(1)).findByUuid("98dc57b3-7a06-4f19-8d4c-0932e9651610");
        verify(documentRepository, times(1)).delete(any(Document.class));
    }

    @Test
    public void owner_should_be_able_to_view_document() {
        boolean result = documentService.isUserAllowedToViewDocument("98dc57b3-7a06-4f19-8d4c-0932e9651610", normalUser);

        assertTrue(result);
    }

    @Test
    public void admin_should_be_able_to_view_document() {
        boolean result = documentService.isUserAllowedToViewDocument("98dc57b3-7a06-4f19-8d4c-0932e9651610", admin);

        assertTrue(result);
    }

    @Test
    public void non_owner_should_not_be_able_to_view_document() {
        boolean result = documentService.isUserAllowedToViewDocument("98dc57b3-7a06-4f19-8d4c-0932e9651610", normalUser2);

        assertFalse(result);
    }
}
