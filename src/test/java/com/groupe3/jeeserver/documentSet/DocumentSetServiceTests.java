package com.groupe3.jeeserver.documentSet;

import com.groupe3.jeeserver.document.model.Document;
import com.groupe3.jeeserver.documentSet.model.DocumentSet;
import com.groupe3.jeeserver.documentSet.repository.DocumentSetRepository;
import com.groupe3.jeeserver.documentSet.service.DocumentSetService;
import com.groupe3.jeeserver.exception.DocumentSetAlreadyExistsException;
import com.groupe3.jeeserver.exception.DocumentSetNotFoundException;
import com.groupe3.jeeserver.user.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class DocumentSetServiceTests {
    @Mock
    private DocumentSetRepository documentSetRepository;

    @InjectMocks
    private DocumentSetService documentSetService;

    private final String adminUsername = "admin";
    private User adminUser;
    private User user1;
    private User user2;

    private DocumentSet set1;
    private DocumentSet set2;

    @Before
    public void init() {
        adminUser = new User();
        adminUser.setUuid("admin");
        adminUser.setUsername(adminUsername);

        user1 = new User();
        user1.setUsername("Willy");
        user1.setUuid("Willy");

        user2 = new User();
        user2.setUsername("On the metal");
        user2.setUuid("BuildOnTheMetal");

        set1 = new DocumentSet();
        set1.setName("Willy_set1");
        set1.setUuid("set1");

        set2 = new DocumentSet();
        set2.setName("on_the_metal_set2");
        set2.setUuid("set2");

        user1.addDocumentSet(set1);

        user2.addDocumentSet(set2);
    }

    @Test
    public void get_all_should_return_all_document_sets() {
        when(documentSetRepository.findAll()).thenReturn(List.of(set1, set2));

        assertEquals(List.of(set1, set2), documentSetService.getAll());
    }

    @Test
    public void get_all_by_users_should_return_users_documents() {
        when(documentSetRepository.findByNameStartsWith(any(String.class))).thenReturn(Optional.of(List.of(set1)));
        Optional<List<DocumentSet>> result = documentSetService.getAllByUser(user1.getUsername());

        assertTrue(result.isPresent());
        assertEquals(List.of(set1), result.get());
    }

    @Test
    public void get_all_by_users_should_return_empty_optional() {
        when(documentSetRepository.findByNameStartsWith(any(String.class))).thenReturn(Optional.empty());
        Optional<List<DocumentSet>> result = documentSetService.getAllByUser(user1.getUsername());

        assertTrue(result.isEmpty());
    }

    @Test
    public void find_by_name_should_return_a_document_set() {
        when(documentSetRepository.findByName(any(String.class))).thenReturn(Optional.of(set1));

        Optional<DocumentSet> result = documentSetService.findByName(set1.getName());

        assertTrue(result.isPresent());
        assertEquals(set1.getUuid(), result.get().getUuid());
    }

    @Test
    public void find_by_name_should_return_empty_optional() {
        when(documentSetRepository.findByName(any(String.class))).thenReturn(Optional.empty());

        Optional<DocumentSet> result = documentSetService.findByName(set1.getName());

        assertTrue(result.isEmpty());
    }

    @Test
    public void find_by_uuid_should_return_a_document_set() {
        when(documentSetRepository.findByUuid(any(String.class))).thenReturn(Optional.of(set1));

        Optional<DocumentSet> result = documentSetService.findByUuid(set1.getUuid());

        assertTrue(result.isPresent());
        assertEquals(set1.getName(), result.get().getName());
    }

    @Test
    public void find_by_uuid_should_return_empty_optional() {
        when(documentSetRepository.findByUuid(any(String.class))).thenReturn(Optional.empty());

        Optional<DocumentSet> result = documentSetService.findByUuid(set1.getUuid());

        assertTrue(result.isEmpty());
    }

    @Test
    public void owner_should_be_able_to_see_the_documentSet() {
        assertTrue(documentSetService.checkDocumentSetOwner(set1, user1));
    }

    @Test
    public void admin_should_be_able_to_see_the_documentSet() {
        documentSetService.setAdminUsernameForMock(adminUsername);

        assertTrue(documentSetService.checkDocumentSetOwner(set1, adminUser));
    }

    @Test
    public void non_owner_should_not_be_able_to_see_the_documentSet() {
        assertFalse(documentSetService.checkDocumentSetOwner(set1, user2));
    }

    @Test
    public void create_should_work() {
        when(documentSetRepository.existsByName(any(String.class))).thenReturn(false);
        when(documentSetRepository.save(any(DocumentSet.class))).thenAnswer(fx -> fx.getArguments()[0]);

        DocumentSet created = documentSetService.create("Test");

        assertEquals("test", created.getName());
        verify(documentSetRepository, times(1)).save(any(DocumentSet.class));
    }

    @Test(expected = DocumentSetAlreadyExistsException.class)
    public void create_already_existing_documentSet_should_throw_an_exception() {
        when(documentSetRepository.existsByName(any(String.class))).thenReturn(true);

        documentSetService.create("Test");
    }

    @Test
    public void add_document_to_set_should_work() {
        when(documentSetRepository.save(any(DocumentSet.class))).thenAnswer(fx -> fx.getArguments()[0]);
        when(documentSetRepository.findByUuid(any(String.class))).thenReturn(Optional.of(set1));

        Document doc = new Document();
        doc.setUuid("doc");
        user1.addDocument(doc);

        documentSetService.addDocument(set1.getUuid(), doc);

        assertTrue(set1.getDocuments().contains(doc));
        assertEquals(set1.getUuid(), doc.getDocumentSet().getUuid());
    }

    @Test(expected = DocumentSetNotFoundException.class)
    public void add_to_non_existent_documentSet_should_raise_exception() {
        when(documentSetRepository.findByUuid(any(String.class))).thenReturn(Optional.empty());

        Document doc = new Document();
        doc.setUuid("doc");
        user1.addDocument(doc);

        documentSetService.addDocument(set1.getUuid(), doc);
    }

    @Test
    public void remove_from_documentSet_should_work() {
        when(documentSetRepository.save(any(DocumentSet.class))).thenAnswer(fx -> fx.getArguments()[0]);
        when(documentSetRepository.findByUuid(any(String.class))).thenReturn(Optional.of(set1));

        Document doc = new Document();
        doc.setUuid("doc");
        user1.addDocument(doc);

        set1.addDocument(doc);

        assertTrue(set1.getDocuments().contains(doc));
        assertEquals(set1.getUuid(), doc.getDocumentSet().getUuid());

        documentSetService.removeDocument(set1.getUuid(), doc);

        assertFalse(set1.getDocuments().contains(doc));
    }

    @Test
    public void convertToElasticSearchCase_should_work() { // Not a very inspired name I know...
        assertEquals("test", documentSetService.convertToElasticSearchCase("TeST"));
        assertEquals("i_am_a_test", documentSetService.convertToElasticSearchCase("I am a test"));
        assertEquals("x___________", documentSetService.convertToElasticSearchCase("_\\/*\"?<>|,#:"));
        assertEquals("x--", documentSetService.convertToElasticSearchCase("---"));
        assertEquals("x--", documentSetService.convertToElasticSearchCase("_--"));
        assertEquals("x--", documentSetService.convertToElasticSearchCase(" --"));
        assertEquals("x--", documentSetService.convertToElasticSearchCase(".--"));
        assertEquals("x--", documentSetService.convertToElasticSearchCase("+--"));
    }
}
